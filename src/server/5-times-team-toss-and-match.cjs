const path=require('path')
const matchFile = path.join(__dirname,'../data/matches.csv');
const csv = require('csvtojson')

function teamWonTossAndMatch() {

   return csv().fromFile(matchFile)
    .then((matches) =>
    {

      
      const teamWonToss=matches.reduce((accumulator, match) =>
      {
        //console.log(match.toss_winner);
        
        if(match.toss_winner===match.winner) 
        {
          if(match.toss_winner in accumulator )
          {
            accumulator[match.toss_winner] +=1;
          }else{
            accumulator[match.toss_winner] =1;
          } 
        }    
        //console.log(accumulator);
        return accumulator;
      },{});
      
      return teamWonToss;
  
  })
  .catch((err) => {
    console.error('Error reading CSV file:', err);
    return {};
  });
}
module.exports=teamWonTossAndMatch;