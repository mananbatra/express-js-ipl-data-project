const path=require('path')
const matchFile = path.join(__dirname,'../data/matches.csv');
const deliveriesFile = path.join(__dirname,'../data/deliveries.csv');
const csv = require('csvtojson');

function calculateStrikeRates() {
  return csv()
    .fromFile(matchFile)
    .then((matches) => {
      let seasonsAndMatchIds = matches.reduce((result, match) => {
        result[match.id] = match.season;
        return result;
      }, {});

      return csv()
        .fromFile(deliveriesFile)
        .then((deliveries) => {
          let batsmanStrikeRates = deliveries
            .map((delivery) => {
              delivery.match_id = seasonsAndMatchIds[delivery.match_id];
              return delivery;
            })
            .reduce((result, delivery) => {
              let ball = 1;
              let totalBalls = 0;
              let totalRuns = 0;

              if (delivery.wide_runs !== '0') {
                ball = 0;
              }

              if (result[delivery.batsman]) {
                if (result[delivery.batsman][delivery.match_id]) {
                  result[delivery.batsman][delivery.match_id]['runs'] +=
                    +delivery.batsman_runs;
                  result[delivery.batsman][delivery.match_id]['balls'] += ball;
                } else {
                  result[delivery.batsman][delivery.match_id] = {};
                  result[delivery.batsman][delivery.match_id]['runs'] =
                    +delivery.batsman_runs;
                  result[delivery.batsman][delivery.match_id]['balls'] = ball;
                }
              } else {
                result[delivery.batsman] = {};
                result[delivery.batsman][delivery.match_id] = {};
                result[delivery.batsman][delivery.match_id]['runs'] =
                  +delivery.batsman_runs;
                result[delivery.batsman][delivery.match_id]['balls'] = ball;
              }

              totalBalls = result[delivery.batsman][delivery.match_id]['balls'];
              totalRuns = result[delivery.batsman][delivery.match_id]['runs'];

              result[delivery.batsman][delivery.match_id].StrickeRate = (
                (totalRuns / totalBalls) *
                100
              ).toFixed(2);

              return result;
            }, {});

          return batsmanStrikeRates;
        });
    })
    .catch((err) => {
      console.error('Error:', err);
      return {};
    });
}

module.exports = calculateStrikeRates;
