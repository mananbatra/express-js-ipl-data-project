const path=require('path')
const csvFilePath = path.join(__dirname,'../data/matches.csv');
const csv = require('csvtojson')

function matchesWonPerTeamPerYear() {

  return csv().fromFile(csvFilePath)
    .then((matches) => {
      //console.log(matches);

      let yearOfIPL = matches.reduce((acc, match) => {
        const year = match.season;
        const matchWinner = match.winner;
        if (year in acc) {
          if (matchWinner in acc[year]) {
            acc[year][matchWinner] += 1
          }
          else {
            acc[year][matchWinner] = 1
          }
        }
        else {
          acc[year] = {};
        }
        return acc;

      }, {})

      //console.log(yearOfIPL);
      return yearOfIPL;

    })
    .catch((err) => {
      console.error('Error reading CSV file:', err);
      return {};
    });
}
module.exports=matchesWonPerTeamPerYear;
