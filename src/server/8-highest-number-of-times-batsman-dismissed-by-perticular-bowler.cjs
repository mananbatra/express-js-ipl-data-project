const path = require('path')
const deliveriesFile = path.join(__dirname, '../data/deliveries.csv');
const csv = require('csvtojson')

function highestTimePlayerDismissAnotherPlayer() {

  return csv().fromFile(deliveriesFile)
    .then((deliveries) => {

      let dismissalPerBowler = Object.entries(deliveries.reduce((accumulator, delivery) => {

        if (delivery.dismissal_kind === 'caught' || delivery.dismissal_kind === 'bowled' || delivery.dismissal_kind === 'lbw' || delivery.dismissal_kind === 'caught and bowled' || delivery.dismissal_kind === 'stumped') {
          let name = `${delivery.bowler}+${delivery.player_dismissed}`;
          //console.log(name)
          if (name in accumulator) {
            accumulator[name] += 1

          } else {
            accumulator[name] = 1
          }
        }

        return accumulator;

      }, {})).sort((playerA, playerB) => playerB[1] - playerA[1]).slice(0, 1);


      bolwerName = dismissalPerBowler[0][0].split("+");

      return (bolwerName[0] + " dismissed " + bolwerName[1] + " " + dismissalPerBowler[0][1] + " times");

    })
    .catch((err) => {
      console.error('Error reading CSV file:', err);
      return {};
    });

}

module.exports=highestTimePlayerDismissAnotherPlayer;