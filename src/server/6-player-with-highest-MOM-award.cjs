const path=require('path')
const csvFilePath = path.join(__dirname,'../data/matches.csv');
const csv = require('csvtojson')

function mostManOfMatchPerYear() {

   return csv().fromFile(csvFilePath)
    .then((matches) =>
    {

      //console.log(matches);
      
      const yearOfIPL=matches.reduce((accumulator, match) =>
      {
        const year=match.season;
        const playerOfMatch=match.player_of_match;
        
        if(!(year in accumulator))
        {
          accumulator[year]={};
        }
        if(!(playerOfMatch in accumulator[year]))
        {
          accumulator[year][playerOfMatch]=0;
        }
        
        accumulator[year][playerOfMatch]++;
        
        
        return accumulator;
        
      },{})
      
      
      const mostPOM=Object.entries(yearOfIPL).reduce((accumulator, year) =>
      {
        const playerObj= Object.entries(year[1])
        .sort((player1,player2) =>
        {
          return player2[1] -player1[1];
        })
        .slice(0,1);
        
        accumulator[year[0]]= Object.fromEntries(playerObj);
        
        return accumulator;
        
      },{});
    
   return mostPOM;
})
.catch((err) => {
  console.error('Error reading CSV file:', err);
  return {};
});
}
module.exports=mostManOfMatchPerYear;