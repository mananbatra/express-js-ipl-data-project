const path=require('path')
const matchFile = path.join(__dirname,'../data/matches.csv');
const deliveriesFile = path.join(__dirname,'../data/deliveries.csv');
const csv = require('csvtojson')

function topTenEconomicBowler(year) {

  let matchIdArray = [];
  csv().fromFile(matchFile)
    .then((matches) => {
      matchIdArray = matches
        .filter((match) => match.season === year)
        .map((match) => match.id);
      //console.log(matchIdArray);

    });
  return csv().fromFile(deliveriesFile)
    .then((deliveries) => {

      const bestBowlerEconomy = Object.entries(deliveries.reduce((acc, currentDelivery) => {
        if (matchIdArray.includes(currentDelivery.match_id)) {
          if (acc[currentDelivery.bowler]) {
            acc[currentDelivery.bowler].balls += 1;
            acc[currentDelivery.bowler].runs += Number(currentDelivery.total_runs);
          } else {
            acc[currentDelivery.bowler] = {
              balls: 1,
              runs: Number(currentDelivery.total_runs)
            };
          }
        }
        return acc;

      }, {}))
        .map(([bowler, { runs, balls }]) =>
        ({
          bowler,
          economy: (runs / balls) * 6
        }))
        .sort((bowlerA, bowlerB) => bowlerA.economy - bowlerB.economy)
        .slice(0, 10);

      return bestBowlerEconomy;

    })
    .catch((err) => {
      console.error('Error reading CSV file:', err);
      return {};
    });
}
module.exports=topTenEconomicBowler;