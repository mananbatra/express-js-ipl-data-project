const path=require('path')
const matchFile = path.join(__dirname,'../data/matches.csv');
const deliveriesFile = path.join(__dirname,'../data/deliveries.csv');
const csv = require('csvtojson')

function extraRunPerTeam2016(year) {

    let matchIdArray=[];
    csv().fromFile(matchFile)
    .then((matches) =>
    {
        matchIdArray=matches
        .filter((match) => match.season === year)
        .map((match) => match.id);
    })

   return csv().fromFile(deliveriesFile)
    .then((deliveries) =>
    {


    //console.log(matchIdArray)

    let extraRunPerTeam=deliveries.reduce((acc , currentDelivery) =>
    {
        if(matchIdArray.includes(currentDelivery.match_id))
        {
            if(currentDelivery.bowling_team in acc)
            {
                acc[currentDelivery.bowling_team] += Number(currentDelivery.extra_runs);
            }else{
                acc[currentDelivery.bowling_team] = Number(currentDelivery.extra_runs);
            }
        }
        return acc;
    },{});

    //console.log(extraRunPerTeam);
    return extraRunPerTeam;
})
.catch((err) => {
    console.error('Error reading CSV file:', err);
    return {};
  });
}
module.exports=extraRunPerTeam2016