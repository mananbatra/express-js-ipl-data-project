const path=require('path')
const csvFilePath = path.join(__dirname,'../data/matches.csv');
const csv = require('csvtojson');

function matchesPerYear() {
  return csv()
    .fromFile(csvFilePath)
    .then((matches) => {
      const yearOfIPL = matches.reduce((accumulator, matchObj) => {
        const { season } = matchObj;
        if (season in accumulator) {
          accumulator[season] += 1;
        } else {
          accumulator[season] = 1;
        }
        return accumulator;
      }, {});

      return yearOfIPL;
    })
    .catch((err) => {
      console.error('Error reading CSV file:', err);
      return {};
    });
}

module.exports = matchesPerYear;
