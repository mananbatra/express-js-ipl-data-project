const path = require('path')
const csvFilePath = path.join(__dirname, '../data/deliveries.csv');
const csv = require('csvtojson')


function bowlerWithBestEconomy() {
    
    return csv().fromFile(csvFilePath)
    .then((deliveries) =>
    {

        // console.log(data);
        
        const bowlerWithBestEconomyInSuperOver = Object.entries(deliveries.filter((item) => {
            return item.is_super_over !== "0";
        }).reduce((accumulator,match) =>
        {
            if(match.bowler in accumulator)
            {
                accumulator[match.bowler]['runs'] += Number(match.total_runs);
                accumulator[match.bowler]['balls'] += 1;
            } else {
                accumulator[match.bowler] = {};
                accumulator[match.bowler]['runs'] = Number(match.total_runs);
                accumulator[match.bowler]['balls'] = 1;
            }
            return accumulator;
        },{})).sort((bowlerA, bowlerB) => {
            const economyA = bowlerA[1].runs / bowlerA[1].balls;
            const economyB = bowlerB[1].runs / bowlerB[1].balls;
            return economyA - economyB;
        }).slice(0,1);
        
        return Object.fromEntries(bowlerWithBestEconomyInSuperOver);
    
})
.catch((err) => {
    console.error('Error reading CSV file:', err);
    return {};
  });
}
module.exports=bowlerWithBestEconomy;