const express = require('express');
const requestId = require('express-request-id');
const { port } = require('./config.js');
const  router  = require('./router/getRouter.js');
const logFileRequest = require('./middleware/logRequest.js')
const unexpectedRequest = require('./middleware/unexpectedRequest.js')
const errorHandler = require('./middleware/errorHandler.js');

const app=express();

app.use(requestId(),logFileRequest);

app.use(router)

app.use(unexpectedRequest)

app.use(errorHandler)

app.listen(port, (error) => {
    if (error) {
        console.error(error);
    } else {
        console.log('Server is running');
    }
});
