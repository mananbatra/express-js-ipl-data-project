const express = require('express');
const path = require('path')
const router = express.Router();

const matchesPerYear = require("../src/server/1-matches-per-year.cjs");
const matchesWonPerTeam = require("../src/server/2-matches-won-per-team-per-year.cjs");
const extraRunPerTeam = require("../src/server/3-extra-runs-per-team-2016.cjs");
const topTenEconomicBowler = require("../src/server/4-top-ten-economic-bowler-2015.cjs");
const teamWonTossAndMatch = require("../src/server/5-times-team-toss-and-match.cjs");
const playerOfMatch = require("../src/server/6-player-with-highest-MOM-award.cjs");
const strikeRate = require("../src/server/7-strike-rate-of-batsman.cjs");
const highestTimePlayerDismissAnotherPlayer = require('../src/server/8-highest-number-of-times-batsman-dismissed-by-perticular-bowler.cjs')
const bowlerWithBestEconomy = require("../src/server/9-bowler-with-best-economy-in-superOver.cjs");
const htmlHomePage = path.join(__dirname, '../homePage.html');
const logFilePath = path.join(__dirname, '../serverLogFile.log');

router.get('/api/matchesPerYear', (req, res, next) => {

    matchesPerYear()
        .then((data) => {
            res.json(data).end();
        })
        .catch((err) => {
            next();
        });
});

router.get('/api/matchesWonPerTeam', (req, res, next) => {

    matchesWonPerTeam()
        .then((data) => {
            res.json(data).end();
        })
        .catch((err) => {
            next();
        });
});

router.get('/api/extraRunPerTeam/:year', (req, res, next) => {
    const { year } = req.params;
    //console.log(year);
    extraRunPerTeam(year)
        .then((data) => {
            res.json(data).end();
        })
        .catch((err) => {
            next();
        });
});

router.get('/api/topTenEconomicBowler/:year', (req, res, next) => {
    const { year } = req.params;
    //console.log(year);
    topTenEconomicBowler(year)
        .then((data) => {
            res.json(data).end();
        })
        .catch((err) => {
            next();
        });
});

router.get('/api/teamWonTossAndMatch', (req, res, next) => {

    teamWonTossAndMatch()
        .then((data) => {
            res.json(data).end();
        })
        .catch((err) => {
            next();
        });
});

router.get('/api/playerOfMatch', (req, res, next) => {

    playerOfMatch()
        .then((data) => {
            res.json(data).end();
        })
        .catch((err) => {
            next();
        });
});

router.get('/api/strikeRate', (req, res, next) => {

    strikeRate()
        .then((data) => {
            res.json(data).end();
        })
        .catch((err) => {
            next();
        });
});

router.get('/api/highestTimePlayerDismissAnotherPlayer', (req, res, next) => {

    highestTimePlayerDismissAnotherPlayer()
        .then((data) => {
            res.json(data).end();
        })
        .catch((err) => {
            next();
        });
});

router.get('/api/bowlerWithBestEconomy', (req, res, next) => {

    bowlerWithBestEconomy()
        .then((data) => {
            res.json(data).end();
        })
        .catch((err) => {
            next();
        });
});

router.get('/', (request, response) => {

    response.sendFile(htmlHomePage)
});

router.get('/logs', (request, response) => {

    response.sendFile(logFilePath)
});

module.exports = router;