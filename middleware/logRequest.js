const fs = require('fs');
const path = require('path');
const logFilePath  = path.join(__dirname, "../serverLogFile.log");


function logFile(request,response,next)
{
    const logData=`Date:${new Date()} | Method:${request.method} | Url: ${request.url} |  ID: ${request.id}\n`;
    fs.appendFile(logFilePath,logData , (error) =>
    {
        if(error){
            next(error);
        }
    });
    next();
}

module.exports=logFile;